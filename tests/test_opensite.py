from typing import Dict

import pytest
from pom.drinks_card import DrinksCard
from pom.mainpage_title import MainpageTitle


@pytest.mark.usefixtures('setup')
class TestsIrlixpub:

    def test_irlixpub(self):
        mainpage_title = MainpageTitle(self.driver)
        text_title = mainpage_title.get_title_text()
        expected_title_text = mainpage_title.TITLE_TEXT
        assert expected_title_text == text_title, 'Заголовок не прошел проверку'

    def test_take_card(self):
        drinks_card = DrinksCard(self.driver)
        card = drinks_card.get_cocktail_card()
        card.click()

        list_ingredients_name = drinks_card.get_text_ingredients_name()
        list_ingredients_value = drinks_card.get_text_ingredients_value()

        dict_ingredients = drinks_card.get_dict_recipes(list_ingredients_name, list_ingredients_value)
        expect_ingredients = drinks_card.INGREDIENT_AND_VALUE

        assert drinks_card.assert_ingredients_items(dict_ingredients, expect_ingredients) == True  #проверка банана
        assert drinks_card.assert_ingredients_keys(dict_ingredients, drinks_card.INGREDIENT) == False  #провекра кукумбера


