

from base.seleniumfind import SeleniumFind
from selenium.webdriver.support.ui import WebDriverWait


class MainpageTitle(SeleniumFind):

    def __init__(self, driver):
        SeleniumFind.__init__(self)
        self.driver = driver
        self.wait = WebDriverWait(driver, 10)
        self.__title_class_name: str = 'title'
        self.TITLE_TEXT = 'Главная'

    def get_title(self):
        return self.is_visible('class_name', self.__title_class_name, 'Main page title element')

    def get_title_text(self) -> str:
        title_text = self.get_title().text
        return title_text


