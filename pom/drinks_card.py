from selenium.webdriver.remote.webelement import WebElement

from base.seleniumfind import SeleniumFind

from selenium.webdriver.support.ui import WebDriverWait

from typing import List





class DrinksCard(SeleniumFind):
    def __init__(self, driver):
        SeleniumFind.__init__(self)
        self.driver = driver
        self.wait = WebDriverWait(driver, 10)
        self.__alco_value: str = '80'
        self.__ingredients_class_name: str = 'dotted-list__item-name'
        self.__ingredients_class_value: str = 'dotted-list__item-value'
        self.INGREDIENT_AND_VALUE = {'banana': '50 ml'}
        self.INGREDIENT = 'cucumber'

    def get_cocktail_card(self) -> WebElement:
        return self.is_visible('partial_link_text', self.__alco_value, 'Cocktail card with alco_value = 80%')

    def get_ingredients_name(self) -> List[WebElement]:
        return self.are_visible('class_name', self.__ingredients_class_name, 'All ingredients name of cocktail')

    def get_text_ingredients_name(self) -> list[str]:
        return self.get_text_from_webelements(self.get_ingredients_name())

    def get_ingredients_value(self) -> List[WebElement]:
        return self.are_visible('class_name', self.__ingredients_class_value, 'All ingredients value of cocktail')

    def get_text_ingredients_value(self) -> list[str]:
        return self.get_text_from_webelements(self.get_ingredients_value())

    def get_dict_recipes(self, ingred_name: list[str], ingred_value: list[str]) -> dict:
        name = ingred_name
        value = ingred_value
        dict_recipe = dict(zip(name, value))
        return dict_recipe

    def assert_ingredients_items(self, dict_ingred: dict, expect_ingred: dict) -> bool:
        print(f"ИНГРЕДИЕНТЫ КОКТЕЙЛЯ: {dict_ingred}")
        print(f"ОЖИДАЕМЫЙ ИНГРЕДИЕНТ:{expect_ingred}")
        result = expect_ingred.items() <= dict_ingred.items()
        if result:
            print("ДАННЫЙ ИНГРЕДИЕНТ В КОКТЕЙЛЕ ЕСТЬ")
        else:
            print("ДАННОГО ИНГРЕДИЕНТА В КОКТЕЙЛЕ НЕТ")
        return result

    def assert_ingredients_keys(self, dict_ingred: dict, expect_ingred: str) -> bool:
        print(f"ИНГРЕДИЕНТЫ КОКТЕЙЛЯ: {dict_ingred}")
        print(f"ОЖИДАЕМЫЙ ИНГРЕДИЕНТ:{expect_ingred}")
        result = expect_ingred in dict_ingred.keys()
        if result:
            print("ДАННЫЙ ИНГРЕДИЕНТ В КОКТЕЙЛЕ ЕСТЬ")
        else:
            print("ДАННОГО ИНГРЕДИЕНТА В КОКТЕЙЛЕ НЕТ")
        return result





# ТУТ Я ЕЩЕ НЕ ЗНАЛ ЧТО В ПИТОНЕ СЛОВАРЬ И ОБЬЕКТ ЭТО ОДНО И ТОЖЕ!  НО РЕШЕНИЕ Я СЧИТАЮ ЭЛЕГАНТНОЕ
# def get_object(self, elements: list[str], element2: list[str]) -> object:
#     obj = {}
#     length_list = len(elements)
#     x = 0
#     while x < length_list:
#         obj[elements[x]] = element2[x]
#         x += 1
#     return obj

# def get_expected_obj(self):
#     s1 = self.recipes('banana', '30 ml')
#     return

object